import { AboutHeader, AboutBody } from './windows/About';
import { LinksHeader, LinksBody } from './windows/Links';
import { ProjectsHeader, ProjectsBody } from './windows/Projects';
import { ContactHeader, ContactBody } from './windows/Contact';
import { GuestbookHeader, GuestbookBody } from './windows/Guestbook';
import { MusicHeader, MusicBody } from './windows/Music';
import { JancukHeader, JancukBody } from './windows/Jancuk';
import { CreditsHeader, CreditsBody } from './windows/Credits';
import { FixHeader, FixBody } from './windows/Fix';

import PippoTheme from '../themes/PippoTheme';
import PippoDarkPro from '../themes/PippoDarkPro';
import PippoBleeding from '../themes/PippoBleeding';

export default function WindowsList() {
  const windowsList = {
    about: {
      hasFullScreen: true,
      opened: false,
      focused: false,
      header: AboutHeader,
      body: AboutBody,
      windowTheme: PippoTheme
    },
    projects: {
      hasFullScreen: true,
      opened: false,
      focused: false,
      header: ProjectsHeader,
      body: ProjectsBody,
      windowTheme: PippoDarkPro
    },
    contact: {
      hasFullScreen: true,
      opened: false,
      focused: false,
      header: ContactHeader,
      body: ContactBody,
      windowTheme: PippoTheme
    },
    links: {
      hasFullScreen: true,
      opened: false,
      focused: false,
      header: LinksHeader,
      body: LinksBody,
      windowTheme: PippoTheme
    },
    guestbook: {
      hasFullScreen: true,
      opened: false,
      focused: false,
      header: GuestbookHeader,
      body: GuestbookBody,
      windowTheme: PippoTheme
    },
    music: {
      hasFullScreen: true,
      opened: false,
      focused: false,
      header: MusicHeader,
      body: MusicBody,
      windowTheme: PippoTheme
    },
    credits: {
      hasFullScreen: true,
      opened: false,
      focused: false,
      header: CreditsHeader,
      body: CreditsBody,
      windowTheme: PippoTheme
    },
    jancuk: {
      hasFullScreen: false,
      opened: false,
      focused: false,
      header: JancukHeader,
      body: JancukBody,
      windowTheme: PippoBleeding
    },
    fixmycomputer: {
      hasFullScreen: true,
      opened: false,
      focused: false,
      header: FixHeader,
      body: FixBody,
      windowTheme: PippoTheme
    },
  };

  return windowsList;
}
