import React, { Component } from 'react';
import moment from 'moment';

import aboutIcon from '../../resources/icons/about.gif';
import avatarPicture from '../../resources/images/myavatar.gif';

import './About.css';

class AboutHeader extends Component {
  render = () => (
    <span>
      <img src={ aboutIcon } alt='about' style={ { height: '15px' } }/> About
    </span>
  )
}

class AboutBody extends Component {
  state = {
    timePassed: '',
  };

  getDateDiff = () => {
    const now = moment();
    const then = moment([2016, 8, 15]);
    const years = now.diff(then, 'year');

    const timePassed = `${years} years`;
    this.setState({ timePassed });
  }

  componentDidMount() {
    this.getDateDiff();
  }

  render = () => {
    const { timePassed } = this.state;
    return (
      <div className='text-container'>
        <img src={ avatarPicture } alt='my avatar' className='avatar-picture' />
        <span style={ { fontWeight: 'bold', fontSize: '1.5em' } }>Hello there!</span>
        <p>
          I'm <b>Sepyan P. Kristanto </b>, you can call me `<i> vian </i>`<br />
          on the internet my nick name is usually <b>vianziro</b>.
        </p>
        <p>
          I was born in <a href='https://en.wikipedia.org/wiki/Denpasar' target='_blank' rel='noopener noreferrer'>Denpasar (Indonesia)</a> <span role="img" aria-label="indonesian-flag">🇮🇩</span> where I lived most of my life. I moved to <a href='https://en.wikipedia.org/wiki/Banyuwangi' target='_blank' rel='noopener noreferrer'>Banyuwangi(Indonesia)</a> <span role="img" aria-label="indonesian-flag">🇮🇩</span> { timePassed } ago where I currently work as a full stack software engineer.
          I have finished my study from <a href='http://istts.ac.id' target='_blank' rel='noopener noreferrer'>Institute of Applied Science and Technology Surabaya</a>.
        </p>
        <p>
          In my free time I like to running and cycling around
          the streets of Banyuwangi.
       ⛰
        </p>
        <p>
          I'm passionate about retrocomputing, Natural Language Processing, Data Mining, Image     Processing.
        </p>
      </div>
    );
  }
}

export { AboutHeader, AboutBody };
