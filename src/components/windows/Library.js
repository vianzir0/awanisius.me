import React, { Component } from 'react';
import {
  Button, Cutout, Anchor, TabBody,
} from 'react95';

import musicIcon from '../../resources/icons/music.gif';

import spotifyIcon from '../../resources/icons/social/spotify.gif';
import bandcampIcon from '../../resources/icons/social/bandcamp.gif';
import soundcloudIcon from '../../resources/icons/social/soundcloud.gif';

import discoveredMusic from '../../resources/recently-discovered-music.json';

import './Music.css';

class MusicHeader extends Component {
  render = () => (
    <span>
      <img src={ musicIcon } alt='icon' style={ { height: '15px' } }/> Music
    </span>
  )
}

class TheMightyMarquee extends React.Component {
  render() {
    // eslint-disable-next-line
    return (<marquee><span className='music-track-text '>{this.props.text}</span></marquee>);
  }
}

class MusicBody extends Component {
  render = () => (<div>
    <div style={ { paddingBottom: '20px' } }>
      I usually save my music on the following online services.
    </div>
    <Cutout className='music-cutout'>
      <div className='music-button'>
        <Anchor
          href='https://open.spotify.com/'
          target='_blank'
          style={ { color: '#000000', textDecoration: 'none' } }
        >
          <Button fullWidth size='lg' style={ { height: '95px', display: 'inline-block', backgroundColor: '#fffee8' } }>
            <img src={ spotifyIcon } className='icon' alt="Spotify link"/>
            <figcaption className='icon-caption'>Spotify</figcaption>
          </Button>
        </Anchor>
      </div>
      <div className='music-button'>
        <Anchor
          href='https://bandcamp.com/vianziro'
          target='_blank'
          style={ { color: '#000000', textDecoration: 'none' } }
        >
          <Button fullWidth size='lg' style={ { height: '95px', display: 'inline-block', backgroundColor: '#fffee8' } }>
            <img src={ bandcampIcon } className='icon' alt="Bandcamp link"/>
            <figcaption className='icon-caption'>Bandcamp</figcaption>
          </Button>
        </Anchor>
      </div>
      <div className='music-button'>
        <Anchor
          href='https://soundcloud.com/vianziro'
          target='_blank'
          style={ { color: '#000000', textDecoration: 'none' } }
        >
          <Button fullWidth size='lg' style={ { height: '95px', display: 'inline-block', backgroundColor: '#fffee8' } }>
            <img src={ soundcloudIcon } className='icon' alt="Bandcamp link"/>
            <figcaption className='icon-caption'>SoundCloud</figcaption>
          </Button>
        </Anchor>
      </div>
    </Cutout>
    <div style={ { paddingBottom: '10px', paddingTop: '20px' } }>
      <TabBody>
        <div style={ { marginTop: '-20px', paddingBottom: '5px' } }>Recently discovered <span role="img" aria-label="headphones">🎧</span></div>
        <Anchor
          href={ discoveredMusic.url }
          target='_blank'
        >
          <Cutout style={ { backgroundColor: 'black', textDecoration: 'none' } }>
            <TheMightyMarquee text={ discoveredMusic.name } />
          </Cutout>
        </Anchor>
      </TabBody>
    </div>
  </div>)
}

export { MusicHeader, MusicBody };
